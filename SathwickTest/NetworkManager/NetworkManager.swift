//
//  NetworkManager.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

class NetworkManager {
    
    static let shared = NetworkManager()
    init() {}
    
    func fetchData<T: Codable>(withURL urlString: String,modal: T.Type, sucess:@escaping(_ userData: T) -> Void, failure: @escaping(_ failuremessage: String) -> Void) -> Void{
        guard let url = URL(string: urlString) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
            guard let data = data else{return}
            do{
                let userModels = try JSONDecoder().decode(modal, from: data)
                sucess(userModels)
            }catch (let error){
                failure(error.localizedDescription)
            }
        }
        task.resume()
        
    }

    
    deinit {
        print("Network Manager Deinitialized")
    }
}
