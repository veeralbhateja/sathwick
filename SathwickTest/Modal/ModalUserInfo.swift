//
//  ModalUserInfo.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//


import Foundation

class ModalUserInfo: Codable{
    let page: Int?
    let per_page: Int?
    let total: Int?
    let total_pages: Int?
    let data: [ModalUserData]?
}

class ModalUserData: Codable {
    let id: Int?
    let email: String?
    let first_name: String?
    let last_name: String?
    let avatar: String?
}
