//
//  InfoDisplayCell.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

class InfoDisplayCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: DownloadableImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    var userID: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImageView.image = #imageLiteral(resourceName: "userImage")
        userName.text = nil
        userEmail.text = nil
        userID = nil
        
    }
    
    public func configure(with info: UserTableViewCellViewModel) {
        if let imageData = info.avatar{
            userImageView.image = UIImage(data: imageData)
            userImageView.layer.cornerRadius = userImageView.bounds.height/2
        }else{
            userImageView.downloadImageFrom(urlString: info.avatarURL ?? "")
        }
        userName.text = info.fullName
        userEmail.text = info.email
        userID = info.id
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
