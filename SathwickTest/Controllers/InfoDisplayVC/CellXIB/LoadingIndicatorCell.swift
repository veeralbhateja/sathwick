//
//  LoadingIndicatorCell.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

class LoadingIndicatorCell: UITableViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
