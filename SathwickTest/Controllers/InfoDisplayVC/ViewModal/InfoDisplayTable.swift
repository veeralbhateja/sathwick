//
//  InfoDisplayTable.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

// MARK: - Custom class to handle TableView DataSource, Delegate and actions in the tableView
class InfoDisplayTable:NSObject {
    
    private var viewModel: InfoDisplayVM?
    
    init(viewModel: InfoDisplayVM) {
        self.viewModel = viewModel
    }
    
}


extension InfoDisplayTable:UITableViewDataSource, UITableViewDelegate{
        
    // MARK: - TableView Row Height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? UITableView.automaticDimension : 50
    }
    
    // MARK:- Number of sections. Section 1 is for Data and 2 for the loader
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // MARK: - Number of Rows in TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? viewModel?.users.value?.count ?? 0 : 1
    }
    
    // MARK: - Designing and filling data in the TableView Cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: InfoDisplayCell.identifier, for: indexPath) as! InfoDisplayCell
            guard let info = viewModel?.users.value else{return UITableViewCell()}
            cell.configure(with: info[indexPath.row])
            return cell
        default:
            // MARK: - To show loader when updating Data after 10 records
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadingIndicatorCell.identifier, for: indexPath) as! LoadingIndicatorCell
//            cell.activityIndicator.stopAnimating()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deleteAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editAction(at: indexPath)
        return UISwipeActionsConfiguration(actions: [edit])
    }

    private func editAction(at indexPath: IndexPath) -> UIContextualAction{
        let edit = UIContextualAction(style: .normal, title: "Edit") {[unowned self] (action, view, actionPerformed: @escaping (Bool) -> ()) in
            self.viewModel?.indexUpdated.value = (indexPath,(self.viewModel?.users.value?[indexPath.row])!)
            actionPerformed(true)
        }
        edit.backgroundColor = UIColor.systemGreen
        
        return edit

    }

    private func deleteAction(at indexPath: IndexPath) -> UIContextualAction{
        let delete = UIContextualAction(style: .destructive, title: "Delete") {[unowned self] (action, view, actionPerformed: @escaping (Bool) -> ()) in
            CoreDataManager.shared.deleteUser(user: (self.viewModel?.users.value?[indexPath.row].user)!, userDeleted: { isDeleted in
                if isDeleted{
                    self.viewModel?.users.value?.remove(at: indexPath.row)
                }
            })
            actionPerformed(true)
        }
        return delete
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let info = viewModel?.users.value else{return}
            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height
            if (offsetY > contentHeight - scrollView.frame.height){
                if !(info.last?.currentPage ?? 0 >= info.last?.totalePages ?? 0){
                    let page: Int = (info.last?.currentPage ?? 0) + 1
                    viewModel?.loadMoreOnScroll.value = (page)
                }
            }
        }
    
}
