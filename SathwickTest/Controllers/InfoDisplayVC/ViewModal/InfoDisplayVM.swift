//
//  InfoDisplayVM.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit
import CoreData

struct InfoDisplayVM {
    
    var users: Observable<[UserTableViewCellViewModel]> = Observable([])
    var loadMoreOnScroll: Observable<Int> = Observable(0)
    var indexUpdated: Observable<(index:IndexPath,user:UserTableViewCellViewModel)> = Observable((IndexPath(),UserTableViewCellViewModel(user: nil)))
    
    func checkForLocalDB(_ page: Int){
        if CoreDataManager.shared.getAllUsers().count > 0{
            self.users.value = CoreDataManager.shared.getAllUsers().compactMap({element in
                UserTableViewCellViewModel(user: element)
            })
        }else{
            getDataForPage(page)
        }
    }
    
    func getDataForPage(_ page: Int){
        NetworkManager.shared.fetchData(withURL: URLManager.getUser + "\(page)", modal: ModalUserInfo.self) {[self] response in
            self.users.value?.append(contentsOf: response.data?.compactMap({element in
                let user = TempUserTableViewCellViewModel(id: "\(element.id ?? 0)", email: element.email, first_name: element.first_name, last_name: element.last_name,avatar:nil,avatarURL: element.avatar, currentPage: response.page, totalePages: response.total_pages)
                return UserTableViewCellViewModel(user: saveResponseInDB(user))
            }) ?? [])
        } failure: { error in
            print(error)
        }
    }
    
    private func saveResponseInDB(_ instance: TempUserTableViewCellViewModel) -> UserInfo{
        let user = UserInfo(context: CoreDataManager.shared.viewContext)
        user.email = instance.email
        user.first_name = instance.first_name
        user.last_name = instance.last_name
        user.avatarURL = instance.avatarURL
        user.avatar = instance.avatar
        user.id = instance.id
        user.currentPage = Int64(instance.currentPage ?? 0)
        user.total_pages = Int64(instance.totalePages ?? 0)
        CoreDataManager.shared.save{_ in}
        return user
    }
    
    func addNewUSer(_ userInfo: UserTableViewCellViewModel? = nil){
        let notVC = Storyboards.Main.instantiate(AddNewUserVC.self)
        if userInfo != nil{
            notVC.userInfo = userInfo
            notVC.isUserUpdated { modal in
                self.users.value = CoreDataManager.shared.getAllUsers().compactMap({element in
                    UserTableViewCellViewModel(user: element)
                })
            }
        }else{
            notVC.isUserAdded { modal in
                if let modal = modal{
                    self.users.value?.append(modal)
                }
            }
        }
        
        Storyboards.Main.present(notVC)
    }
}




private struct TempUserTableViewCellViewModel {
    var id: String?
    var email: String?
    var first_name: String?
    var last_name: String?
    var avatar: Data?
    var avatarURL: String?
    var currentPage:Int?
    var totalePages: Int?
}

struct UserTableViewCellViewModel {
    let user: UserInfo?
    var cId: NSManagedObjectID? {
        return user?.objectID
    }
    var id: String?{
        return user?.id
    }
    var email: String?{
        return user?.email
    }
    var first_name: String?{
        return user?.first_name
    }
    var last_name: String?{
        return user?.last_name
    }
    var avatar: Data?{
        return user?.avatar
    }
    var fullName: String?{
        return (user?.first_name ?? "") + " " + (user?.last_name ?? "")
    }
    
    var avatarURL: String?{
        return user?.avatarURL
    }
    var currentPage:Int?{
        return Int(user?.currentPage ?? 0)
    }
    var totalePages: Int?{
        return Int(user?.total_pages ?? 0)
    }
    
}

