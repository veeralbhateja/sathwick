//
//  InfoDisplayVC.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

class InfoDisplayVC: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var tableViewInfo: UITableView!
    
    private var viewModel = InfoDisplayVM()
    private var tableData: InfoDisplayTable?
    private var isLoading: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK: - Registering TableViewCell
        tableViewInfo.register(InfoDisplayCell.nib, forCellReuseIdentifier: InfoDisplayCell.identifier)
        tableViewInfo.register(LoadingIndicatorCell.nib, forCellReuseIdentifier: LoadingIndicatorCell.identifier)
//        tableViewInfo.register(UserInfoHeader.nib, forHeaderFooterViewReuseIdentifier: UserInfoHeader.identifier)
        
        // MARK: - Initializing TableCustomClass and
        tableData = InfoDisplayTable(viewModel: viewModel)
        tableViewInfo.dataSource = tableData
        tableViewInfo.delegate = tableData
        tableViewInfo.estimatedRowHeight = 100
        tableViewInfo.rowHeight = UITableView.automaticDimension
        
        // MARK: - Calling Network API for first time
        viewModel.checkForLocalDB(1)
        
        // MARK: - Thiis Observer will tell us whenever application will get Data from API
        viewModel.users.bind { [unowned self] _ in
            // MARK: - UI will be updated in Main Queue after we get data
            DispatchQueue.main.async {
                self.isLoading = false
                let cell = self.tableViewInfo.cellForRow(at: IndexPath(row: 0, section: 1)) as? LoadingIndicatorCell
                cell?.activityIndicator.stopAnimating()
                self.tableViewInfo.reloadData()
            }
        }

        // MARK: - This observer will tell Application when application is scrolled and needs moreData for next page. It will also tell us the next page number for which we need data
        viewModel.loadMoreOnScroll.bind {[unowned self] page in
            if !(self.isLoading){
                self.isLoading = true
                DispatchQueue.main.async {
                    let cell = self.tableViewInfo.cellForRow(at: IndexPath(row: 0, section: 1)) as? LoadingIndicatorCell
                    cell?.activityIndicator.startAnimating()
                }
                guard page ?? 0 > 0 else {return}
                self.viewModel.getDataForPage(page ?? 1)
            }
        }
        
        viewModel.indexUpdated.bind {[unowned self] (data) in
            if let user = data?.user, user.cId != nil{
                self.viewModel.addNewUSer(user)
            }
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
}

extension InfoDisplayVC {
    @IBAction func addNewUser(_ sender: UIBarButtonItem){
        viewModel.addNewUSer()
    }
}

