//
//  AddNewUserVC.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit
import CoreData

class AddNewUserVC: UIViewController {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var textFldFirstName: UITextField!
    @IBOutlet weak var textFldLastName: UITextField!
    @IBOutlet weak var textFldEmail: UITextField!
    private var imageData: Data?
    typealias userInfoAdded = (_ modal: UserTableViewCellViewModel?) -> Void
    private var completion:userInfoAdded?
    private var updated:userInfoAdded?
    private var customTFDelegate = CustomNewUserTF()
    private var viewModal = AddNewUserVM()
    var userInfo: UserTableViewCellViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyboardNotification()
        textFldEmail.delegate = customTFDelegate
        textFldLastName.delegate = customTFDelegate
        textFldFirstName.delegate = customTFDelegate
        
        viewModal.imageAdded.bind({[weak self] image in
            guard let img = image?.image else{return}
            self?.imageData = image?.data
            DispatchQueue.main.async {
                self?.userImageView.image = img
            }
        })
        
        userInfo != nil ? isEditable() : nil
        // Do any additional setup after loading the view.
    }
    
    private func isEditable(){
        textFldEmail.text = userInfo?.email
        textFldLastName.text = userInfo?.last_name
        textFldFirstName.text = userInfo?.first_name
        if let data = userInfo?.avatar{
            imageData = data
            userImageView.image = UIImage(data: data)
        }        
    }
    
    
    func isUserAdded(_ completion: userInfoAdded?) {
        self.completion = completion
    }
    func isUserUpdated(_ updated: userInfoAdded?) {
        self.updated = updated
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func imageViewTapped(_ sender: UITapGestureRecognizer){
        DispatchQueue.main.async { [self] in
            viewModal.imageTapped()
        }
    }
    
    @IBAction func actionCancel(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSave(_ sender: UIButton){
        viewModal = AddNewUserVM(firstName: textFldFirstName.text, lastName: textFldLastName.text, email: textFldEmail.text, avatar: imageData, userInfo: userInfo)
        viewModal.save {[unowned self] value in
            if let updated = updated{
                self.dismiss(animated: true) {
                    updated(value)
                }
            }
            if let complete = completion{
                self.dismiss(animated: true) {
                    complete(value)
                }
            }
        }
    }
}
