//
//  AddNewUserVM.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

struct AddNewUserVM {
    
    var firstName: String?
    var lastName: String?
    var email: String?
    var avatar: Data?
    var userInfo: UserTableViewCellViewModel?
    
    typealias imageData = (image:UIImage?,data:Data?)
    var imageAdded: Observable<imageData> = Observable((image:nil,data:nil))
    
    func save(_ savedSuccessfully: (UserTableViewCellViewModel) -> Void){
        if isValiidatedForNil().isEmpty{
            Storyboards.Main.getTopVC()?.showAlert(message: isValiidatedForNil().message)
        }else{
            if let id = userInfo?.cId, let user = CoreDataManager.shared.getUserById(id: id){
                user.email = email
                user.first_name = firstName
                user.last_name = lastName
                user.avatar = avatar
                CoreDataManager.shared.save { isSaved in
                    savedSuccessfully(UserTableViewCellViewModel(user: user))
                }
            }else{
                let user = UserInfo(context: CoreDataManager.shared.viewContext)
                user.email = email
                user.first_name = firstName
                user.last_name = lastName
                user.avatar = avatar
                CoreDataManager.shared.save { isSaved in
                    savedSuccessfully(UserTableViewCellViewModel(user: user))
                }
            }
        }
    }
    
    
    func imageTapped(){
        CustomImagePicker.shared.showPicker(for: [.gallery,.camera]) {[self] image, data in
            imageAdded.value = (image: image, data: data)
        }
    }
    
    private func isValiidatedForNil() -> (isEmpty:Bool,message:String){
        if avatar?.isEmpty ??  true{
            return (isEmpty: true, message: "Select an Avatar")
        }
        if firstName?.isEmpty ??  true{
            return (isEmpty: true, message: "Enter First Name.")
        }
        else if lastName?.isEmpty ??  true{
            return (isEmpty: true, message: "Enter Last Name.")
        }
        else if email?.isEmpty ?? true{
            return (isEmpty: true, message: "Enter an Email.")
        }
        else if !(email?.isValidEmail() ?? false){
            return (isEmpty: true, message: "Not a Valid Email.")
        }
        return (isEmpty: false, message: "")
    }

}


