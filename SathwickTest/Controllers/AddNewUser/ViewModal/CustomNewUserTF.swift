//
//  CustomNewUserTF.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

class CustomNewUserTF: NSObject,UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let topView = Storyboards.Main.getTopVC()?.view else{return true}
        return topView.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            if textField.tag == 2{
                if updatedText.isValidEmail(){
                    textField.textColor = UIColor.black
                }else{
                    textField.textColor = UIColor.systemRed
                }
            }
        }
        return true
    }
    
   

}
