//
//  CustomImagePicker.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit
import MobileCoreServices
import Photos


enum ImagePickerTypes: String{
    case camera = "Camera"
    case gallery = "Gallery"
    case cancel = "Cancel"
}



class CustomImagePicker: NSObject {
    static let shared = CustomImagePicker()
    override init(){}
    
    // MARK: - Constants
    private let imagePickerController = UIImagePickerController()
    
    typealias imageSelected = (UIImage,Data?) -> Void
    // MARK: - Completion Handler
    private var completion:imageSelected?

    func showPicker(for pickerType: [ImagePickerTypes],imagePicked: imageSelected?){
        let viewController = Storyboards.Main.getTopVC()
        var finalAction:[UIAlertAction] = []
        self.completion = imagePicked
        let cancel = getAlertAction(for: ImagePickerTypes.cancel.rawValue, style: .cancel)
        
        for action in pickerType{
            if action.rawValue == ImagePickerTypes.gallery.rawValue{
                let gallary = getAlertAction(for: ImagePickerTypes.gallery.rawValue, selector: {[unowned self] (_) in
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .photoLibrary
                    imagePickerController.allowsEditing = true
                    viewController?.present(imagePickerController, animated: true, completion: nil)
                },style: .default)
                finalAction.append(gallary)
            }
            if action.rawValue == ImagePickerTypes.camera.rawValue && UIImagePickerController.isSourceTypeAvailable(.camera){
                let camera = getAlertAction(for: ImagePickerTypes.camera.rawValue, selector: {[unowned self] (_) in
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .camera
                    imagePickerController.allowsEditing = true
                    viewController?.present(imagePickerController, animated: true, completion: nil)
                },style: .default)
                finalAction.append(camera)
            }
        }
        
        finalAction.append(cancel)
        viewController?.showAlert(title: nil, message: nil, style: .actionSheet, actions: finalAction)
    }
        
    
    private func getAlertAction(for name: String, selector: ((UIAlertAction) -> Void)? = nil, style : UIAlertAction.Style) -> UIAlertAction
    {
        return UIAlertAction(title: name, style: style, handler: selector)
    }
    
    deinit {
        print("ImagePicker deinit called")
    }
}

// MARK:- Image Picker Delegates
extension CustomImagePicker: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            let imgData = pickedImage.jpegData(compressionQuality: 0)
            self.imagePickerController.dismiss(animated: true, completion: {
                if let completion = self.completion{
                    completion(pickedImage,imgData)
                }else{
                    fatalError("No value recieved in Completion")
                }
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
