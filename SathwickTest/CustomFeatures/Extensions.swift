//
//  Extensions.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import Foundation
import UIKit

// MARK:- Adding Custom Functionalities to UIView Class
extension UIView{
    
    // MARK: - Properties
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    @IBInspectable var cornerRadius: Double{
        get{
            return Double(self.layer.cornerRadius)
        }
        set {
            self.clipsToBounds = true
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderWidth: Double{
        get{
            return Double(self.layer.cornerRadius)
        }
        set {
            self.clipsToBounds = true
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var shadowColor: UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor ?? UIColor.white.cgColor)
        }
        set {
            self.layer.shadowColor = newValue.cgColor
            self.layer.shadowOpacity = 0.4
            self.layer.masksToBounds = false
            self.layer.shadowOffset = CGSize(width: -1, height: 1) // .zero
            self.layer.shadowRadius = 10
            self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        }
    }
    

    
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}

extension UIViewController{
    func addKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(_ notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func showAlert(title: String? = "Alert!", message: String?, style: UIAlertController.Style = .alert, actions: [UIAlertAction] = []){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        if actions.count > 0{
            for action in actions{
                alert.addAction(action)
            }
        }else{
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }

}

extension String{
    func isValidEmail() -> Bool {
         let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
         let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
         return emailPred.evaluate(with: self)
     }
}
