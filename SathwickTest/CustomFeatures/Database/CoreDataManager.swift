//
//  CoreDataManager.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import Foundation
import CoreData

class CoreDataManager {
    
    let persistentContainer: NSPersistentContainer
    static let shared = CoreDataManager()
    
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    func getUserById(id: NSManagedObjectID) -> UserInfo? {
        do {
            return try viewContext.existingObject(with: id) as? UserInfo
        } catch {
            return nil
        }
    }
    
    func deleteUser(user: UserInfo,userDeleted: (_ isDeleted:Bool) -> Void) {
        viewContext.delete(user)
        save { isSaved in
            userDeleted(isSaved)
        }
    }
    
    func update(avatar forUrl: String,avatarData: Data){
        if getAllUsers().filter({$0.avatarURL == forUrl}).count > 0{
            getAllUsers().filter({$0.avatarURL == forUrl}).first?.avatar = avatarData
            save {_ in}
        }else{
            print("No URL Found")
        }
        
    }
    
    func getAllUsers() -> [UserInfo] {
        
        let request: NSFetchRequest<UserInfo> = UserInfo.fetchRequest()
        do {
            return try viewContext.fetch(request)
        } catch {
            return []
        }
        
    }
    
    func save(saved: (_ isSaved: Bool) -> Void) {
        do {
            try viewContext.save()
            saved(true)
        } catch {
            saved(false)
            viewContext.rollback()
        }
    }
    
    private init() {
        persistentContainer = NSPersistentContainer(name: "UserAppModal")
        persistentContainer.loadPersistentStores { (description, error) in
            if let error = error {
                fatalError("Unable to initialize Core Data Stack \(error)")
            }
        }
    }
    
}
