//
//  DownloadableImageView.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import UIKit

class DownloadableImageView: UIImageView {

    // MARK: - Constants
    private let imageCache = NSCache<NSString, AnyObject>()

    // MARK: - Properties
    private var imageURLString: String?

    func downloadImageFrom(urlString: String, imageMode: UIView.ContentMode = .scaleAspectFill, isCircular: Bool = true) {
        guard let url = URL(string: urlString) else {return}
        downloadImageFrom(url: url, imageMode: imageMode,isCircular: isCircular)
    }
    
    private var abc: Int = 60

    private func downloadImageFrom(url: URL, imageMode: UIView.ContentMode, isCircular: Bool) {
        contentMode = imageMode
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) as? UIImage {
            self.image = cachedImage
            isCircular == true ? self.layer.cornerRadius = self.bounds.height / 2 : nil
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data)
                    self.imageCache.setObject(imageToCache!, forKey: url.absoluteString as NSString)
                    self.image = imageToCache
                    CoreDataManager.shared.update(avatar: url.absoluteString, avatarData: data)
                    isCircular == true ? self.layer.cornerRadius = self.bounds.height / 2 : nil
                }
            }.resume()
        }
    }
}
