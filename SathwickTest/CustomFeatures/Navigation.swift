//
//  Navigation.swift
//  SathwickTest
//
//  Created by Veeral Bhateja on 31/05/21.
//

import Foundation
import UIKit

public enum Storyboards: String {
    case Main
    
    public func instantiate<VC: UIViewController>(_ viewController: VC.Type,
                            inBundle bundle: Bundle = Bundle.main) -> VC {
        guard
            let vc = UIStoryboard(name: self.rawValue, bundle: Bundle.main).instantiateViewController(withIdentifier:VC.identifier ) as? VC
        else {
            fatalError("Couldn't instantiate \(VC.identifier) from \(self.rawValue)")
        }
        
        return vc
    }
    
    public func present(_ viewController: UIViewController){
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            viewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            topController.present(viewController, animated: true, completion: nil)
        }
    }
    
    public func getTopVC() -> UIViewController?{
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        guard var topController = keyWindow?.rootViewController else{return nil}
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
    
}


extension UIViewController {
    
    public static var defaultNib: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    public static var identifier: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
}

